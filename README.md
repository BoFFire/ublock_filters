# Liste de filtres pour uBlock Origin

Liste personnelle où je répertorie les publicités que je rencontre sur les sites Web que je visite. Généralement des sites Web algériens.

## Comment ajouter la liste ?

1. Installez le plugin uBlock Origin sur votre navigateur. Exemple sur Firefox.
2. Allez dans les paramêtres de uBlock Origin et importez la liste de filtres, tout à fait en bas :

`https://gitlab.com/BoFFire/ublock_filters/-/raw/master/filters.txt`

3. Cliquez sur appliquer.

